<!-- main page section -->
<div class="profileSec">
    <div id="about">
        <h3>About Me</h3>
        <!-- objective statement -->
        <p>Exceptionally creative and dependable Entry Level Web Developer with a stellar customer service record and superb work ethic. Broadly and deeply knowledgeable in a wide variety of computer languages as well as the principles and techniques of website construction and maintenance. Highly adept at conveying complex technical information to a variety of professional and lay audiences in a clear and understandable manner.</p>
    </div>
    <!-- web development skills -->
    <div id="skills" >
        <aside>
            
            <img src="../profile/assets/img/mePic.jpg" class="img-circle" id="mePic"atl=""Jeff Cisneros Pic/>
            
            <!-- work experience -->
            <h3 style="text-align: center; margin-bottom: 8%;">Work Experience</h3>
            <h4>TELCOR</h4>
            <p>Point of Care (POC) Installation Analysts work with the POC team to establish connectivity with new customers or modify existing VPN connections, work on assigned project's to remotely install TELCOR POC software, upgrade / migrate TELCOR POC software at existing customer sites.</p>
            <h4>Fiserv</h4>
            <p>Perform software implementations for Fiserv's clients via telephone, email, and web based contact channels. Deliver professional and timely client communications regarding project updates and product implementation services. Thoroughly track, organize and document all 
product implementation and support related activities. Develop and maintain a full understanding of Fiserv products and services and stay abreast of relevant industry trends and best Practices. Maintained friendly and professional customer interactions at all times.</p>
        </aside>
        <!-- web development skills -->
        <h3 style="padding-top: 10%;">Skills</h3>
        <div class="col-lg-6">
           <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="90"
                     contenteditable=""accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:90%">
                    HTML
                </div>
           </div>
               <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="70"
                     contenteditable="" accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                    CSS
                </div>
            </div>
             <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60"
                     contenteditable="" accesskey=""aria-valuemin="0" aria-valuemax="100" style="width:60%">
                    JavaScript
                </div>
            </div>
            
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="40"
                     contenteditable="" accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                    C# ASP.NET
                </div>
            </div>
        </div>
        
        </div>
    <div class="col-lg-6">
                <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60"
                     contenteditable="" accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:60%">
                    JQuery
                </div>
                </div>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="40"
                     contenteditable="" accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:40%">
                    PHP
                </div>
            </div>
          
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="70"
                     contenteditable="" accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                    React
                </div>
            </div>
                        <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="60"
                     contenteditable="" accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                    Redux
                </div>
            </div>
                        <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="85"
                     contenteditable="" accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                    MERN Stack (MongoDB, ExpressJS, ReactJS, NodeJS)
                </div>
            </div>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="70"
                     contenteditable="" accesskey="" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                    SQL
                </div>
            </div>
        </div>
    <!-- web projects links -->
    <div id="projects" style="margin-top: 20%;">
        <h3>Web Projects</h3>
        <h5>MeterSpot Website (ASP.NET MVC)</h5>
        <a href="http://meterspot.apphb.com/">http://meterspot.apphb.com/</a>
        <h5>MeterSpot App (iOS App)</h5>
        <a href="https://mockup.io/#projects/139377/mockups">https://mockup.io/#projects/139377/mockups</a>
        <h5>CodePen (Front End Project)</h5>
        <a href="https://codepen.io/NebMex/project/full/ZywEeA">https://codepen.io/NebMex/project/full/ZywEeA</a>
        <h5>Social Media Web App (MERN Stack)</h5>
<a href="hhttps://cuseme-client-es.herokuapp.com/" target="_blank">CuseMe</a>    
     </div>
    </div>


