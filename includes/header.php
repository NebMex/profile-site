<?php
$tpTitle="Jeff Cisneros Profile";
$pgHeading="Web Developer";
$pgDesc="My Skills";
$pgKeyword="Jeff Cisneros";



?>




<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- page description -->
    <meta name="description" content="<?php echo $pgDesc ?>">
    <!-- keyword search -->
    <meta name="keywords" content="<?php echo $pgKeyword ?>">
    <!-- title -->    
    <title><?php echo $tpTitle ?></title>
    <!-- style sheets -->
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/css/styleMain.css">
</head>
<body>
    <div id="wrapper">
        <!--Side bar-->
      <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">Jeff Cisneros
                        <i class="fa fa-arrow-right"></i>
                    </a>
                </li>
                <li><a href="<?php echo ROOT_URL; ?>">Home</a></li>
                <li><a href="<?php echo ROOT_URL; ?>#about">About</a></li>
                <li><a href="<?php echo ROOT_URL; ?>#skills">Skills</a></li>
                <li><a href="<?php echo ROOT_URL; ?>#skills">Work Experience</a></li>
                <li><a href="<?php echo ROOT_URL; ?>#projects">Projects</a></li>
            </ul>
        </div>
    
    <!-- Page Content -->
    
<div id="page-content-wrapper">
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <!-- menu icon -->
                <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" id="menu"></i></a>
                <!-- download resume file -->
                <a href="JeffCisneros.php" id="resume">Download Resume</a>
                <!-- main header text and social media links -->
                <div id="headings">
                    <h1 id="nameHeading"> <?php echo 'Jeff Cisneros'; ?> </h1>
                    <h3 class="webDevHeading"><?php echo 'Web Developer'; ?></h3>
                    <div id="socialIcon">
                        <!-- facebook link -->
                        <a href="https://www.facebook.com/jeff.cisneros.52"><i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i></a>&nbsp;
                        <!-- linked in link -->
                        <a href="https://www.linkedin.com/in/jeff-cisneros-84539350"><i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i></a>&nbsp;
                        <a href="https://bitbucket.org/NebMex/"><i class="fa fa-bitbucket fa-2x" aria-hidden="true"></i></a>&nbsp;
                        <!-- github link -->
                        <a href="https://github.com/jcisn30"><i class="fa fa-github fa-2x" aria-hidden="true"></i></a>&nbsp;
                        <!-- twitter link -->
                        <a href="https://twitter.com/jcisner1"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                    <h3 class="webDevHeading"><?php echo 'Profile'; ?></h3>
                    <a href="#about"><i class="fa fa-arrow-down fa-2x" id="arrow" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    </div>
</div>

