
<!DOCTYPE html>
<html <?php echo 'lang="en-US"';?>>

    <!-- add header section -->
    <?php include 'includes/header.php'; ?>
    <div class="container">

     
      
     	<?php require($view); ?>
     

    </div>
    <!-- add footer section -->
   <?php include 'includes/footer.php'; ?>

<!-- scripts section -->
<script src="<?php echo ROOT_PATH; ?>assets/js/jquery.min.js"></script>
<script src="<?php echo ROOT_PATH; ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo ROOT_PATH; ?>assets/js/javascript.js"></script>
</body>
</html>