<?php
//Start Session
session_start();

// Include Config
require('config.php');


require('classes/Profile.php');
require('classes/Controller.php');
require('classes/Model.php');

require('controllers/home.php');
//require('controllers/shares.php');
//require('controllers/users.php');

require('models/home.php');
//require('models/share.php');
//require('models/user.php');

$Profile = new Profile(INPUT_GET);
$controller = $Profile->createController();
if($controller){
	$controller->executeAction();
}

